FROM python:3.10.4-alpine

WORKDIR /app

COPY . .

RUN apk update && apk add
RUN pip install --no-cache-dir -r requirements.txt


EXPOSE 80

CMD ["uvicorn", "--host", "0.0.0.0", "--port", "80", "src.main:app"]
