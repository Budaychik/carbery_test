from pydantic import BaseModel


class Table(BaseModel):
    duration: float
    distance: float
