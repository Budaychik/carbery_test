from pydantic import BaseModel


class Route(BaseModel):
    first_point: str
    second_point: str
    time: float
