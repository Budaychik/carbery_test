from clients.ABC.routing_engine import RoutingEngineClient
from schemas.coordinates import Coordinates


class OpenRouteServiceClient(RoutingEngineClient):
    def get_route(self, first_point: Coordinates, second_point: Coordinates):
        pass

    def get_table(self):
        pass

    def get_nearest(self):
        pass
