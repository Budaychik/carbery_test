from abc import ABC, abstractmethod
from typing import List

from schemas.coordinates import Coordinates
from schemas.table import Table
from schemas.route import Route


class RoutingEngineClient(ABC):

    @abstractmethod
    def get_route(self, first_point: Coordinates, second_point: Coordinates) -> Route:
        pass

    @abstractmethod
    def get_table(self, coordinates: List[Coordinates]) -> Table:
        pass

    @abstractmethod
    def get_nearest(self):
        pass
