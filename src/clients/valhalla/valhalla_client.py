from typing import List
import aiohttp
import json
from clients.ABC.routing_engine import RoutingEngineClient
from schemas.coordinates import Coordinates
from schemas.table import Table
from schemas.route import Route


class ValhallaClient(RoutingEngineClient):

    def __init__(self, url: str):
        self.url = url

    async def get_route(self, first_point: Coordinates, second_point: Coordinates) -> Route:
        """
        Возвращает координаты точек и время пути.
        :param first_point: Принимает координаты первой точки;
        :param second_point: Принимает координаты второй точки;
        """

        param = {"locations": [{"lat": first_point.latitude, "lon": first_point.longitude},
                               {"lat": second_point.latitude, "lon": second_point.longitude}],
                 "costing": "auto", "units": "km",
                 "format": "json", "id": "Quick route"}

        json_param = json.dumps(param)
        params = {"json": json_param.replace(" ", "")}
        async with aiohttp.ClientSession() as session:
            async with session.get(self.url + '/route', params=params) as response:
                response_json = await response.json()
                route = Route(
                    first_point=f"{response_json['trip']['locations'][0]['lat']}, {response_json['trip']['locations'][0]['lon']}",
                    second_point=f"{response_json['trip']['locations'][1]['lat']}, {response_json['trip']['locations'][1]['lon']}",
                    time=response_json['trip']['summary']['time'])
                return route

    async def get_table(self, coordinates: List[Coordinates]) -> Table:
        """
        Возвращает продолжительность и дистанцию маршрута между координатами.
        :param coordinates: Принимает список координат;
        """
        param = {"locations": [{"lat": coordinates[0].latitude, "lon": coordinates[0].longitude},
                               {"lat": coordinates[1].latitude, "lon": coordinates[1].longitude}],
                 "costing": "auto", "units": "km",
                 "format": "osrm"}

        json_param = json.dumps(param)
        params = {"json": json_param.replace(" ", "")}
        async with aiohttp.ClientSession() as session:
            async with session.get(self.url + '/route', params=params) as response:
                response_json = await response.json()
                table = Table(duration=response_json['routes'][0]['duration'],
                              distance=response_json['routes'][0]['distance'])
                return table

    def get_nearest(self):
        """Возвращает ближайшие места к координатам."""
        pass
