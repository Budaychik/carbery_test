from typing import List

from clients.ABC.routing_engine import RoutingEngineClient
from clients.valhalla.valhalla_client import ValhallaClient
from config import config
from exceptions.region_not_found import RegionNotFoundException
from schemas.coordinates import Coordinates
from services.region_service import get_region_by_coordinates


def get_routing_engine_client_by_coordinates(coordinates: List[Coordinates]) -> RoutingEngineClient:
    """Возвращает объект класса ValhallaClient, OSRMClient, OpenRouteServiceClient"""
    region = get_region_by_coordinates(coordinates[0])
    service_data = config.get(region)
    if service_data is None:
        raise RegionNotFoundException(status_code=404)
    if service_data['route-engine'] == 'valhalla':
        return ValhallaClient(service_data['url'])
