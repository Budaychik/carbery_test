config = {
    "Ural Federal District": {
        "url": "http://localhost:3333",
        "route-engine": "osrm"
    },

    "Siberian Federal District": {
        "url": "http://92.255.174.30:8002",
        "route-engine": "valhalla"
    },

    "Central Federal District": {
        "url": "http://localhost:2345",
        "route-engine": "openrouteservice"
    }
}
