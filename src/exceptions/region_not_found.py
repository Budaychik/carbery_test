from fastapi import HTTPException


class RegionNotFoundException(HTTPException):
    """Исключение вызывается, если регион не обслуживается"""
    def __str__(self):
        return "Данный регион не обслуживает ни один сервис!"
