from typing import List, Any
from fastapi import APIRouter, FastAPI
from clients.routing_engine import get_routing_engine_client_by_coordinates
from schemas.table import Table
from schemas.route import Route
from src.schemas.coordinates import Coordinates

app = FastAPI()
router = APIRouter(prefix="/routing")
app.include_router(router)


@app.post("/route")
async def route(coordinates: List[Coordinates]) -> Route:
    """
    Возвращает координаты точек и время пути.

    :param first_point: Принимает координаты первой точки;

    :param second_point: Принимает координаты второй точки;
    """
    client = get_routing_engine_client_by_coordinates(coordinates)
    return await client.get_route(first_point=coordinates[0], second_point=coordinates[1])


@app.post("/table")
async def table(coordinates: List[Coordinates]) -> Table:
    """
    Возвращает продолжительность и дистанцию маршрута между координатами.

    :param coordinates: Принимает список координат;
    """

    client = get_routing_engine_client_by_coordinates(coordinates)
    return await client.get_table(coordinates)


@app.post("/nearest")
async def nearest(coordinates: Coordinates) -> Any:
    """
    Возвращает ближайшие места к координатам.
    """
