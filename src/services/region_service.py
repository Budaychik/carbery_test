from geopy import Nominatim

from schemas.coordinates import Coordinates


def get_region_by_coordinates(coordinates: Coordinates, language='en') -> str:
    """
    Определяет регион по переданным координатам

    :param coordinates: Принимает координаты точки;
    :param language: Язык вывода данных
    """
    geo = Nominatim(user_agent='carbery')
    point = f"{coordinates.latitude}, {coordinates.longitude}"
    region = geo.reverse(point, language=language).raw['address']['region']
    return region
